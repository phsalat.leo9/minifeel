// main banner script
// ===============================================================
// main banner script
var prodMainBnrswiper = new Swiper(".prodMainBnrSwiper", {
    slidesPerView: 1,
    loop: true,

    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },

    autoplay: {
        delay: 3000,
    },

});


// product listing filter script
// ====================================================================
var filterActive;

function filterCategory(category) {
    if (filterActive != category) {

        // reset results list
        $('.filter-cat-results .f-cat').removeClass('active');

        // elements to be filtered
        $('.filter-cat-results .f-cat')
            .filter('[data-cat="' + category + '"]')
            .addClass('active');

        // reset active filter
        filterActive = category;
    }
}

$('.f-cat').addClass('active');

$('.filtering select').change(function () {
    if ($(this).val() == 'cat-all') {
        $('.filter-cat-results .f-cat').addClass('active');
        filterActive = 'cat-all';
    } else {
        filterCategory($(this).val());
    }
});

// prod listing page accordion 
$("#accordion").accordion({
    heightStyle: "content",
    active: false,
    collapsible: true,
    header: "div.accordianheader"
});




// product gallery script
// ====================================================================
$('#thumbnail-slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 210,
    itemMargin: 15,
    direction: "vertical",
    asNavFor: '#slider'
});

$('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    itemWidth: 610,
    slideshow: false,
    sync: "#thumbnail-slider"
});

// rating star script
$(document).ready(function () {

    /* 1. Visualizing things on Hover - See next part for action on click */
    $('#stars li').on('mouseover', function () {
        var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

        // Now highlight all the stars that's not after the current hovered star
        $(this).parent().children('li.star').each(function (e) {
            if (e < onStar) {
                $(this).addClass('hover');
            } else {
                $(this).removeClass('hover');
            }
        });

    }).on('mouseout', function () {
        $(this).parent().children('li.star').each(function (e) {
            $(this).removeClass('hover');
        });
    });


    /* 2. Action to perform on click */
    $('#stars li').on('click', function () {
        var onStar = parseInt($(this).data('value'), 10); // The star currently selected
        var stars = $(this).parent().children('li.star');

        for (i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass('selected');
        }

        for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass('selected');
        }

    });
});



// detail page accordion script
$(document).ready(function () {
    //toggle the component with class accordion_body
    $(".accordion_head").click(function () {
        if ($('.accordion_body').is(':visible')) {
            $(".accordion_body").slideUp(300);
            $(".plusminus").text('+');
        }
        if ($(this).next(".accordion_body").is(':visible')) {
            $(this).next(".accordion_body").slideUp(300);
            $(this).children(".plusminus").text('+');
        } else {
            $(this).next(".accordion_body").slideDown(300);
            $(this).children(".plusminus").text('-');
        }
    });
});



// trending product swiper
var similarswiper = new Swiper(".similarSwiper", {
    pagination: {
        el: ".pagination-design-3",
        clickable: true

    },

    breakpoints: {

        480: {
            slidesPerView: 1,
            spaceBetween: 0
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 20
        },
        990: {
            slidesPerView: 2,
            spaceBetween: 20
        },
        1024: {
            slidesPerView: 4,
            spaceBetween: 30
        },
    }

});


// heart script


$(document).ready(function () {
    $("#heart").click(function () {
        if ($("#heart").hasClass("liked")) {
            $("#heart").html('<i class="fa fa-heart-o" aria-hidden="true"></i>');
            $("#heart").removeClass("liked");
        } else {
            $("#heart").html('<i class="fa fa-heart" aria-hidden="true"></i>');
            $("#heart").addClass("liked");
        }
    });
});