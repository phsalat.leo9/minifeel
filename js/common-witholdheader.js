/*=========== Sticky Header Starts ===============*/
function fixedHeader() {
    var sticky = $('#header'),
        scroll = $(window).scrollTop();
    if (scroll >= 50) sticky.addClass('fixHeader');
    else sticky.removeClass('fixHeader');
}
$(window).scroll(function (e) {
    fixedHeader();
});
fixedHeader();


// /* load html */
$('#header').load('header.html', function () {
    fixedHeader();

    var offerHeight = $(".top-advrt-panel").outerHeight();
    var headHeight = $(".home-header").outerHeight();
    var totalHeight = offerHeight + headHeight;

    $(document).on('click', '.advrt-close-btn', function () {
        $(".top-advrt-panel").slideUp();
        $('#header').addClass('fixHeaderNew');

    });
});

$('#footer').load('footer.html');


// $(document).ready(function () {
//     $(window).scrollTop(0);
// });

// mobile menu js
$(document).on('click', '.side-menu', function () {
    $('body').addClass('overflowHidden');
    $('.mobile-menu').addClass('in');
    $('.menuOverlay').addClass('in');
});
$(document).on('click', '.close-menu,.menuOverlay,.dropdown-link', function () {
    $('body').removeClass('overflowHidden');
    $('.mobile-menu').removeClass('in');
    $('.menuOverlay').removeClass('in');
});

// dropdown mobile toggle script
$(document).on('click', 'body', function (e) {
    $('.drop ul').removeClass("active");
});

$(document).on('click', '.drop', function (e) {
    e.stopPropagation();
    if (!$(this).find('ul').hasClass('active')) {
        $('.drop ul').removeClass("active");
        $(this).find('ul').addClass("active");
    } else {
        $('.drop ul').removeClass("active");
    }
});

window.CanvasRenderer = class {
    setProjectInterface() {}
}


// main banner script
var mainBnrswiper = new Swiper(".mainBnrSwiper", {
    slidesPerView: 1,
    loop: true,

    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },

    autoplay: {
        delay: 3000,
    },

});


AOS.init();