 // before after script
 var divisor = document.getElementById("divisor"),
     handle = document.getElementById("handle"),
     slider = document.getElementById("slider");

 function moveDivisor() {
     handle.style.left = slider.value + "%";
     divisor.style.width = slider.value + "%";
 }

 window.onload = function () {
     moveDivisor();
 };


 //  accordion script

 $('.toggle').click(function (e) {
     e.preventDefault();

     let $this = $(this);

     if ($this.next().hasClass('show')) {
         $this.next().removeClass('show');
         $this.next().slideUp(350);
     } else {
         $this.parent().parent().find('li .inner').removeClass('show');
         $this.parent().parent().find('li .inner').slideUp(350);
         $this.next().toggleClass('show');
         $this.next().slideToggle(350);
     }
 });