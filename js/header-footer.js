/*================================= Sticky Header Starts =================================*/
function fixedHeader() {
    var sticky = $('#header'),
        scroll = $(window).scrollTop();
    if (scroll >= 10) sticky.addClass('fixHeader');
    else sticky.removeClass('fixHeader');
}
$(window).scroll(function (e) {
    fixedHeader();
});
fixedHeader();
/* Sticky Header Ends */


$('#header').load('header.html');
$('#footer').load('footer.html');


$(document).ready(function () {
    $(window).scrollTop(0);
});


// mobile menu js
$(document).on('click', '.side-menu', function () {
    $('body').addClass('overflowHidden');
    $('.mobile-menu').addClass('in');
    $('.menuOverlay').addClass('in');
});
$(document).on('click', '.close-menu,.menuOverlay,.dropdown-link', function () {
    $('body').removeClass('overflowHidden');
    $('.mobile-menu').removeClass('in');
    $('.menuOverlay').removeClass('in');
});


// dropdown mobile toggle script

$(document).on('click', 'body', function (e) {
    $('.drop ul').removeClass("active");
});

$(document).on('click', '.drop', function (e) {
    e.stopPropagation();
    if (!$(this).find('ul').hasClass('active')) {
        $('.drop ul').removeClass("active");
        $(this).find('ul').addClass("active");
    } else {
        $('.drop ul').removeClass("active");
    }
});