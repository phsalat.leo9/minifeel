/*=========== Sticky Header Starts ===============*/
function fixedHeader() {
    var sticky = $('#header'),
        scroll = $(window).scrollTop();
    if (scroll >= 50) sticky.addClass('fixHeader');
    else sticky.removeClass('fixHeader');
}
$(window).scroll(function (e) {
    fixedHeader();
});
fixedHeader();

// /* load header html */
$('#header').load('header.html', function () {
    fixedHeader();

    var offerHeight = $(".top-advrt-panel").outerHeight();
    var headHeight = $(".home-header").outerHeight();
    var totalHeight = offerHeight + headHeight;

    $(document).on('click', '.advrt-close-btn', function () {
        $(".top-advrt-panel").slideUp();
        $('#header').addClass('fixHeaderNew');
    });

    /*  MOBILE MENU */
    if ($(window).width() < 1060) {

        // mobile menu
        $(document).on('click', '.hamburger', function () {
            $(this).toggleClass('h-active');
            $('.main-nav').toggleClass('slidenav');
            $('.blk-bg ').toggleClass('block');
            $('html,body').toggleClass('ov-hid');
        });

        $('.header-home .main-nav ul li  a').click(function (event) {
            $('.hamburger').removeClass('h-active');
            $('.main-nav').removeClass('slidenav');
            $('html,body').removeClass('ov-hid');
        });

        $('.blk-bg ').click(function (event) {
            $(this).removeClass('block');
            $('.hamburger').removeClass('h-active');
            $('.main-nav').removeClass('slidenav');
            $('html,body').removeClass('ov-hid');

        });
    }

    $(".main-nav a").on('click', function (event) {
        $('.submenu').slideUp();

        if ($(this).hasClass('down')) {
            $('.main-nav a').removeClass('down');
            $('.submenu').slideUp();
        } else {
            $(this).addClass('down');
            $(this).parent().find('.submenu').slideDown();
        }
    });

    $(".close-btn").click(function () {
        $(".searchmenu").hide();
        $('html,body').removeClass('ov-hid');

    });
    $(".search-drop-down img").click(function () {
        $(".searchmenu").show();
    });

});



$('#footer').load('footer.html');

window.CanvasRenderer = class {
    setProjectInterface() {}
}




AOS.init();