// home page script
// =========================================================================

// main banner script
var mainBnrswiper = new Swiper(".mainBnrSwiper", {
    slidesPerView: 1,
    loop: true,

    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },

    autoplay: {
        delay: 3000,
    },

});


// advrt swiper script
var advrtswiper = new Swiper(".advrtSwiper", {
    slidesPerView: 1,
    centeredSlides: true,
    loop: true,
    spped: 800,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false
    },

});


// clients script
var clientswiper = new Swiper(".clientsSwiper", {

    loop: true,
    speed: 3500,
    autoplay: {
        delay: 0,
        disableOnInteraction: false
    },

    breakpoints: {
        320: {
            slidesPerView: 2.5,
            spaceBetween: 20
        },
        480: {
            slidesPerView: 2,
            spaceBetween: 10,
        },
        768: {
            slidesPerView: 3,
        },
        1024: {
            slidesPerView: 4
        },
        1200: {
            slidesPerView: 6.5,
            spaceBetween: 0
        }
    }
});

// experience swiper script
var experienceswiper = new Swiper(".experienceSwiper", {
    slidesPerView: 1.5,
    spaceBetween: 20,
    loop: true,

    centeredSlides: true,
    pagination: {
        el: ".pagination-design-2",
        clickable: true
    },
    spped: 800,

    autoplay: {
        delay: 3000,
        disableOnInteraction: false
    },


    loop: true,

    // breakpoints: {
    //     480: {
    //         slidesPerView: 1,
    //         spaceBetween: 30
    //     },
    //     768: {
    //         slidesPerView: 1
    //     },
    //     1024: {
    //         slidesPerView: 1.5,
    //         spaceBetween: 30,
    //     },

    // }

});

// bestseller Swiper swiper
var bestsellerswiper = new Swiper(".bestsellerSwiper", {
    pagination: {
        el: ".pagination-design-3",
        clickable: true

    },
    loop: true,

    autoplay: {
        delay: 3000,
        disableOnInteraction: false
    },

    breakpoints: {
        360: {
            slidesPerView: 1.5,
            spaceBetween: 20
        },
        640: {
            slidesPerView: 2,
            spaceBetween: 20
        },
        960: {
            slidesPerView: 3,
            spaceBetween: 20
        },
        1200: {
            slidesPerView: 4,
            spaceBetween: 30
        },
    }

});

// trending product swiper
var trendingswiper = new Swiper(".trendingSwiper", {
    pagination: {
        el: ".pagination-design-3",
        clickable: true

    },
    loop: true,

    autoplay: {
        delay: 3000,
        disableOnInteraction: false
    },

    breakpoints: {
        360: {
            slidesPerView: 1.5,
            spaceBetween: 20
        },
        640: {
            slidesPerView: 2,
            spaceBetween: 20
        },
        960: {
            slidesPerView: 3,
            spaceBetween: 20
        },
        1200: {
            slidesPerView: 4,
            spaceBetween: 30
        },
    }

});

// offer swiper
var offerswiper = new Swiper(".offerSwiper", {
    slidesPerView: 3,
    // spaceBetween: 30,
    // centeredSlides: true,
    loop: true,

    autoplay: {
        delay: 3000,
        disableOnInteraction: false,
    },

    breakpoints: {
        360: {
            slidesPerView: 1,
            spaceBetween: 20
        },
        640: {
            slidesPerView: 2,
            spaceBetween: 20
        },
        960: {
            slidesPerView: 3,
            spaceBetween: 20
        },
    }
});


// category Swiper script
var categoryswiper = new Swiper(".categorySwiper", {

    loop: true,

    breakpoints: {
        360: {
            slidesPerView: 1.5,
            spaceBetween: 10
        },
        480: {
            slidesPerView: 2,
            spaceBetween: 10
        },
        768: {
            slidesPerView: 3
        },

    }
});


// bestselling Swiper script
var bestsellingswiper = new Swiper(".bestsellingSwiper", {
    loop: true,

    breakpoints: {
        360: {
            slidesPerView: 1.5,
            spaceBetween: 10
        },
        480: {
            slidesPerView: 2,
            spaceBetween: 20
        },
        768: {
            slidesPerView: 3,
            spaceBetween: 20
        },

    }
});


// testimonial swiper
var testiswiper = new Swiper(".testimonialSwiper", {
    // slidesPerView: 3.5,
    // spaceBetween: 30,
    loop: true,
    // centeredSlides: true,

    autoplay: {
        delay: 3000,
        disableOnInteraction: false,
    },

    breakpoints: {

        360: {
            slidesPerView: 1,
            spaceBetween: 20,
        },

        576: {
            slidesPerView: 2,
            spaceBetween: 20
        },

        640: {
            slidesPerView: 2,
            spaceBetween: 20
        },
        768: {
            slidesPerView: 3,
            spaceBetween: 20
        },
        1024: {
            slidesPerView: 2.5,
            spaceBetween: 20
        },
        1200: {
            slidesPerView: 3.5,
            spaceBetween: 30
        },
    }

});

// reel swiper
var reelswiper = new Swiper(".reelSwiper", {
    // slidesPerView: 1.5,
    // spaceBetween: 60,
    // centeredSlides: true,
    loop: true,

    autoplay: {
        delay: 3000,
        disableOnInteraction: false,
    },

    breakpoints: {
        1600: {
            slidesPerView: 1,
            spaceBetween: 60
        },
        1660: {
            slidesPerView: 1.5,
            spaceBetween: 60
        },
    }
});


// before after script
var divisor = document.getElementById("divisor"),
    handle = document.getElementById("handle"),
    slider = document.getElementById("slider");

function moveDivisor() {
    handle.style.left = slider.value + "%";
    divisor.style.width = slider.value + "%";
}

window.onload = function () {
    moveDivisor();
};


// line animation 

let tl = gsap.timeline({
    // yes, we can add it to an entire timeline!
    scrollTrigger: {
        trigger: ".offerSwiper",
        start: "0 50%", // when the top of the trigger hits the top of the viewport
        end: "0 50%", // end after scrolling 500px beyond the start
        toggleActions: "play none none none",
        // markers: true
    }
});

// add animations and labels to the timeline
tl.to(".animated-line", {
    scaleY: 1,
    duration: 4
})